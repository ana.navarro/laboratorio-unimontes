<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'laboratorios' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_+m6f6kZCl.:8rlB{0:4!cV:cgJ<6 ;Q1XIQz5-Y?j{Oc$`U?w?#~7qu)*2~-:?|' );
define( 'SECURE_AUTH_KEY',  'ZqGt7:Yydc?x~{>{?~S:SsH*i)xFxCI%Wd]QpQV`HFcu+4Q.tn%V%Kc*h I!)5Se' );
define( 'LOGGED_IN_KEY',    't7Y~v-0s^o|al=j{c4S$(^k::e:Z%}aevZD r6Y6C( {gf.q}mZYJOe{b.{+AUf$' );
define( 'NONCE_KEY',        'zR^Rq?Ujy~R`{{/<NC]DTCi;T{MyFxTchCO<]|7B m+FapZmTuNKbB3T[?DL.fY^' );
define( 'AUTH_SALT',        '2MtoR 7=w!u1M,^abVP/6&(nW9ln6x(5iW;*cPu<g r/+@p1%afgWJaOep,B/nMH' );
define( 'SECURE_AUTH_SALT', 'y*PlE#~@kxk^kRoF2/?JaGSW7p*@E|sSI%!1cB0~f+#EZli2Ftrg`8PyVL(j8@2n' );
define( 'LOGGED_IN_SALT',   '{gp&$-iBFAX(FCF_ Ui}+e%H$u<lo0fY^K}1(E?JCrwFi-,th3h0jeTR7g^;.T]i' );
define( 'NONCE_SALT',       ' bHXHO=tr-@_otVV}4= Xp{xi :Ji9Db!W|]W(s8f_Jx&sWGcKxI0PT}P4[Px_SA' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/laboratorio/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* Isto é tudo, pode parar de editar! :) */
/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
